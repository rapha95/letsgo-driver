angular.module('app.routes', [])

.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider
    

      .state('menu.letSGo', {
    url: '/page1',
    views: {
      'side-menu21': {
        templateUrl: 'templates/letSGo.html',
        controller: 'letSGoCtrl'
      }
    }
  })

  .state('menu', {
    url: '/side-menu21',
    templateUrl: 'templates/menu.html',
    controller: 'menuCtrl'
  })

  .state('login', {
    url: '/pageLogin',
    templateUrl: 'templates/login.html',
    controller: 'loginCtrl'
  })

  .state('signup', {
    url: '/pageCadastro',
    templateUrl: 'templates/signup.html',
    controller: 'signupCtrl'
  })

  .state('menu.perfil', {
    url: '/pagePerfil',
    views: {
      'side-menu21': {
        templateUrl: 'templates/perfil.html',
        controller: 'perfilCtrl'
      }
    }
  })

  .state('menu.corridas', {
    url: '/pageCorridas',
    views: {
      'side-menu21': {
        templateUrl: 'templates/corridas.html',
        controller: 'corridasCtrl'
      }
    }
  })

  .state('menu.suporte', {
    url: '/pageSuporte',
    views: {
      'side-menu21': {
        templateUrl: 'templates/suporte.html',
        controller: 'suporteCtrl'
      }
    }
  })

  .state('menu.configuraEs', {
    url: '/pageConfig',
    views: {
      'side-menu21': {
        templateUrl: 'templates/configuraEs.html',
        controller: 'configuraEsCtrl'
      }
    }
  })

  .state('mapsExample', {
    url: '/page10',
    templateUrl: 'templates/mapsExample.html',
    controller: 'mapsExampleCtrl'
  })

$urlRouterProvider.otherwise('/pageLogin')


});