cordova.define('cordova/plugin_list', function(require, exports, module) {
module.exports = [
    {
        "file": "plugins/cordova-plugin-dialogs/www/notification.js",
        "id": "cordova-plugin-dialogs.notification",
        "pluginId": "cordova-plugin-dialogs",
        "merges": [
            "navigator.notification"
        ]
    },
    {
        "file": "plugins/cordova-plugin-dialogs/www/browser/notification.js",
        "id": "cordova-plugin-dialogs.notification_browser",
        "pluginId": "cordova-plugin-dialogs",
        "merges": [
            "navigator.notification"
        ]
    },
    {
        "file": "plugins/cordova-plugin-firebase-realtime-database/www/firebase-browser.js",
        "id": "cordova-plugin-firebase-realtime-database.FirebaseDatabasePlugin",
        "pluginId": "cordova-plugin-firebase-realtime-database",
        "clobbers": [
            "FirebaseDatabasePlugin"
        ]
    }
];
module.exports.metadata = 
// TOP OF METADATA
{
    "cordova-plugin-whitelist": "1.3.3",
    "cordova-plugin-geolocation": "4.0.1",
    "cordova-plugin-dialogs": "2.0.1",
    "cordova-plugin-firebase-realtime-database": "0.0.2"
}
// BOTTOM OF METADATA
});