angular.module('app.controllers', [])
  
.controller('letSGoCtrl', ['$scope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams) {
    mapfit.apikey = "591dccc4e499ca0001a4c6a4246491cfb4d34e6aaab6747d8da77abe";
    // draw map
    let map = mapfit.MapView('mapfit', {theme: 'day'});
    map.drawMap();
    //add recenter button
    map.setRecenterButtonEnabled(true);
    getPosition(); //gets user position

    //allow pinch for zoom level

    //map.showUserLocation();
    //globally used variables
    var mylocation;
    var mydestination;
    var stopAdress;
    var totalCost=0;
    var latTrack;
    var longTrack;
    var RTP;
    var watchposition;
    var firebaseDriver= window.FirebaseDatabasePlugin.ref('Driver');
    
//begins the trip showing the cost
    var searchTrip = document.getElementById("startNav");
    searchTrip.addEventListener("click",startSearch)
    function startSearch() {
        firebaseDriver.updateChildren({
            'lat' : latTrack,
            'long' : longTrack,
            'car' : 'UberX',
            'tripStatus': 'f' //free
        });
        
    }
        var latlongtoAdress; //makes latitude and longitute var to be replaced as address



    var options = {
        enableHighAccuracy: true,
        timeout: 10000,
        maximumAge: 3000
      };
      
      function success(pos) {
        var crd = pos.coords;
        lat = crd.latitude;
        long = crd.longitude;
      
        console.log('Sua posição atual é:');
        console.log('Latitude : ' + crd.latitude);
        console.log('Longitude: ' + crd.longitude);
        console.log('Mais ou menos ' + crd.accuracy + ' metros.');
        RTP = mapfit.Marker([crd.latitude,crd.longitude]);
        latlongtoAdress= new mapfit.LatLng(crd.latitude,crd.longitude);
        //creates a real time position
        let myIcon = mapfit.Icon();
        myIcon.setIconUrl('sports');
        RTP.setIcon(myIcon); //Real Time Position tracker. Updates the position every X seconds.
        map.addMarker(RTP);
        map.setCenter(RTP);
        mylocation=RTP;
        trackPosition();
      };
      
      function error(err) {
        console.warn('ERROR(' + err.code + '): ' + err.message);
      };
      function getPosition(){
        navigator.geolocation.getCurrentPosition(success, error, options);
      }
     
      //real time tracking test without mapfit

      function successTracking(pos) {
          console.log(pos);
        var crd = pos.coords;
        var latlongAdress= new mapfit.LatLng(crd.latitude,crd.longitude);
        RTP.setPosition(latlongAdress);
        map.setCenter(RTP);
        latTrack = crd.latitude;
        longTrack = crd.longitude;

        //trackPosition();
      };

      function trackPosition()
      {
         watchposition = navigator.geolocation.watchPosition(successTracking,error,options);
          console.log("moved");
      }
      //setInterval(trackPosition,5000);
      
      
      
      

    
/*
    //give info about a location
    let placeInfo=mapfit.PlaceInfo();
    placeInfo.setTitle('Local onde escorrem as lágrimas');
    placeInfo.setDescription('<p>E salgam meu</br> café</p>')/
//set zoom
    map.setZoom(18); 
    let work = mapfit.Marker([-25.441541,-49.2979679]);
    //attribute the info to the marker
    work.setPlaceInfo(placeInfo);
    //set the map center on marker position
    map.setCenter(work);
    let utfpr= mapfit.Marker([-25.4392969,-49.2710649]);
    map.addMarker(work);
    map.addMarker(utfpr);
    //creates a variable in order to keep both markers in the same view
    let museums = mapfit.Layer();
    museums.add(work);
    museums.add(utfpr);
    //creates a layer allowing to see both markers
    map.addLayer(museums);
    //centers the view with the markers on the edge
    map.setCenterWithLayer(museums, 90, 90);
    //draw a line from the start marker to the last one
    //let myPolyline = mapfit.Polyline([[-25.441541,-49.2979679],[-25.4392969,-49.2710649]]);
    //map.addPolyline(myPolyline);
    //map.setCenterWithLayer(myPolyline,0 ,0);*/
    /*var mark =document.getElementById("marker");

    mark.addEventListener("click",markpoint)
    function markpoint(){
    
        let originMarker = mapfit.Marker([-25.4392969,-49.2710649]);
        //originMarker.adress = 'batel, curitiba';
        map.addMarker(originMarker);
        map.setCenter(originMarker);

    }
    var mark2 =document.getElementById("marker2");

    mark2.addEventListener("click",markpoint2)
    function markpoint2(){
    
        let originMarker = mapfit.Marker([-25.441541,-49.2979679]);
        //originMarker.adress = 'batel, curitiba';
        map.addMarker(originMarker);
        map.setCenter(originMarker);

    }

    var mark3 = document.getElementById("marker3");
    mark3.addEventListener("click",markpoint3)
   */ 
    //here we calculate the trip between the start and end point
    function calculateRoute(){
        let key = "591dccc4e499ca0001a4c6a4480a0f60043e40c7a03ceaa33e309973"
        let directionsReq = new mapfit.Directions(key, "https://api.mapfit.com/v2");

        let encodedRoute;
    
        let pointsArray
        directionsReq.route(latlongtoAdress,mydestination, 'driving') 
            .then(function(data) {
              encodedRoute = data.trip.legs[0].shape;
              pointsArray = decodePolyline(encodedRoute)
              let directionPolyline = mapfit.Polyline(pointsArray)
              map.addPolyline(directionPolyline)
              map.setCenterWithLayer(directionPolyline, 10, 40);
              calculateCost(data);
             // console.log("Dados da rota=",data);
            })
      
          function decodePolyline(encoded) {
              let polyLineArrayCompiled = [];
              var index = 0,
                  len = encoded.length;
              var lat = 0,
                  lng = 0;
              while (index < len) {
                  var b, shift = 0,
                      result = 0;
                  do {
                      b = encoded.charAt(index++).charCodeAt(0) - 63;
                      result |= (b & 0x1f) << shift;
                      shift += 5;
                  } while (b >= 0x20);

                  var dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
                  lat += dlat;
                  shift = 0;
                  result = 0;

                  do {
                      b = encoded.charAt(index++).charCodeAt(0) - 63;
                      result |= (b & 0x1f) << shift;
                      shift += 5;
                  } while (b >= 0x20);
                  var dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
                  lng += dlng;

                  polyLineArrayCompiled.push([(lat / 1E6), (lng / 1E6)])
              }
              //console.log(polyLineArrayCompiled)
              

            return polyLineArrayCompiled

          }
}
    
//creates a simple marker on the map
    function createMarker(marker){
        let Marker = mapfit.Marker();
        Marker.address = marker;
        map.addMarker(Marker);
    }
    //updates origin
        /*var d = document.getElementById("destination");
        d.addEventListener("keyup",getDestination)
        function getDestination(){
            d = document.getElementById("destination");
            console.log(d.value);

        }*/
        
        
    


   /* mapfit.apikey = "591dccc4e499ca0001a4c6a4480a0f60043e40c7a03ceaa33e309973";
      let key = "591dccc4e499ca0001a4c6a4480a0f60043e40c7a03ceaa33e309973"
      let map = mapfit.MapView('mapfit', {theme: 'day'})

          let directionsReq = new mapfit.Directions(key, "https://api.mapfit.com/v2");

          let encodedRoute;
      
          let pointsArray
          // start marker
          let startMarkerIcon = mapfit.Icon();
          startMarkerIcon.setIconUrl('commercial');
          let startMarker = mapfit.Marker();
          startMarker.address = 'praca do japao, curitiba';
          startMarker.setIcon(startMarkerIcon);
          map.addMarker(startMarker)
          // end marker 
          let endMarker = mapfit.Marker();
          endMarker.address = 'avenida sete de setembro 1830,curitiba';
          map.addMarker(endMarker)
          directionsReq.route('praca do japao, curitiba','avenida sete de setembro 1830,curitiba', 'walking')
            .then(function(data) {
              encodedRoute = data.trip.legs[0].shape;
              pointsArray = decodePolyline(encodedRoute)
              let directionPolyline = mapfit.Polyline(pointsArray)
              map.addPolyline(directionPolyline)
              map.setCenterWithLayer(directionPolyline, 10, 40);
            })
      
          function decodePolyline(encoded) {
              let polyLineArrayCompiled = [];
              var index = 0,
                  len = encoded.length;
              var lat = 0,
                  lng = 0;
              while (index < len) {
                  var b, shift = 0,
                      result = 0;
                  do {
                      b = encoded.charAt(index++).charCodeAt(0) - 63;
                      result |= (b & 0x1f) << shift;
                      shift += 5;
                  } while (b >= 0x20);

                  var dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
                  lat += dlat;
                  shift = 0;
                  result = 0;

                  do {
                      b = encoded.charAt(index++).charCodeAt(0) - 63;
                      result |= (b & 0x1f) << shift;
                      shift += 5;
                  } while (b >= 0x20);
                  var dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
                  lng += dlng;

                  polyLineArrayCompiled.push([(lat / 1E6), (lng / 1E6)])
              }
              console.log(polyLineArrayCompiled.toString());
            return polyLineArrayCompiled

          }*/
          
 
}])
   
.controller('menuCtrl', ['$scope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams) {


}])
   
.controller('loginCtrl', ['$scope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams) {


}])
   
.controller('signupCtrl', ['$scope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams) {


}])
   
.controller('perfilCtrl', ['$scope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams) {


}])
   
.controller('corridasCtrl', ['$scope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams) {


}])
   
.controller('suporteCtrl', ['$scope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams) {


}])
   
.controller('configuraEsCtrl', ['$scope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams) {
    
    $scope.idiomas = [
    {
        'id' : 'pt',
        'label': 'PT'
    },
    {
      'id' :'eng',
      'label': 'ENG'
    }
    ];
    
    $scope.data = {
    
    'idiom' : $scope.idiomas[0].id
        
    }
    
}
])
   
.controller('mapsExampleCtrl', ['$scope', 'uiGmapGoogleMapApi', function($scope, uiGmapGoogleMapApi) {
    // Do stuff with your $scope.
    // Note: Some of the directives require at least something to be defined originally!
    // e.g. $scope.markers = []

    // uiGmapGoogleMapApi is a promise.
    // The "then" callback function provides the google.maps object.
    uiGmapGoogleMapApi.then(function(maps){
        // Configuration needed to display the road-map with traffic
        // Displaying Ile-de-france (Paris neighbourhood)
        $scope.map = {
            center: {
              latitude: -23.598763,
              longitude: -46.676547
            },
            zoom: 13,
            options: {
                mapTypeId: google.maps.MapTypeId.ROADMAP, // This is an example of a variable that cannot be placed outside of uiGmapGooogleMapApi without forcing of calling the google.map helper outside of the function
                streetViewControl: false,
                mapTypeControl: false,
                scaleControl: false,
                rotateControl: false,
                zoomControl: false
            }, 
            showTraficLayer:true
        };
    });
}])
 